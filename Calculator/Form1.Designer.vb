﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtLeftValue = New System.Windows.Forms.TextBox()
        Me.txtRightValue = New System.Windows.Forms.TextBox()
        Me.chkSaveOutput = New System.Windows.Forms.CheckBox()
        Me.cmbOperations = New System.Windows.Forms.ComboBox()
        Me.lblResult = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuspendLayout()
        '
        'txtLeftValue
        '
        Me.txtLeftValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLeftValue.Location = New System.Drawing.Point(12, 27)
        Me.txtLeftValue.Name = "txtLeftValue"
        Me.txtLeftValue.Size = New System.Drawing.Size(383, 45)
        Me.txtLeftValue.TabIndex = 0
        '
        'txtRightValue
        '
        Me.txtRightValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRightValue.Location = New System.Drawing.Point(405, 27)
        Me.txtRightValue.Name = "txtRightValue"
        Me.txtRightValue.Size = New System.Drawing.Size(383, 45)
        Me.txtRightValue.TabIndex = 1
        '
        'chkSaveOutput
        '
        Me.chkSaveOutput.AutoSize = True
        Me.chkSaveOutput.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSaveOutput.Location = New System.Drawing.Point(405, 107)
        Me.chkSaveOutput.Name = "chkSaveOutput"
        Me.chkSaveOutput.Size = New System.Drawing.Size(227, 43)
        Me.chkSaveOutput.TabIndex = 4
        Me.chkSaveOutput.Text = "Save Output"
        Me.chkSaveOutput.UseVisualStyleBackColor = True
        '
        'cmbOperations
        '
        Me.cmbOperations.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbOperations.FormattingEnabled = True
        Me.cmbOperations.Items.AddRange(New Object() {"Add", "Multiply", "Divide", "Substract"})
        Me.cmbOperations.Location = New System.Drawing.Point(13, 107)
        Me.cmbOperations.Name = "cmbOperations"
        Me.cmbOperations.Size = New System.Drawing.Size(383, 46)
        Me.cmbOperations.TabIndex = 3
        '
        'lblResult
        '
        Me.lblResult.AutoSize = True
        Me.lblResult.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.lblResult.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResult.ForeColor = System.Drawing.Color.Brown
        Me.lblResult.Location = New System.Drawing.Point(13, 193)
        Me.lblResult.Name = "lblResult"
        Me.lblResult.Size = New System.Drawing.Size(0, 39)
        Me.lblResult.TabIndex = 5
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.lblResult)
        Me.Controls.Add(Me.cmbOperations)
        Me.Controls.Add(Me.chkSaveOutput)
        Me.Controls.Add(Me.txtRightValue)
        Me.Controls.Add(Me.txtLeftValue)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtLeftValue As TextBox
    Friend WithEvents txtRightValue As TextBox
    Friend WithEvents chkSaveOutput As CheckBox
    Friend WithEvents cmbOperations As ComboBox
    Friend WithEvents lblResult As Label
    Friend WithEvents ToolTip1 As ToolTip
End Class
